﻿/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------Question 2 ------------------------------------------------------*/
/*---------------------------------Learning outcomes 1 & 2 - Data Types and Recurssion------------------------------------*/
/*----------------------------------------------------Marks - Out of 10----------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
/*------------------------------------------------------------------------------------------------------------------------*/
/*-----------Question 2 - Implement a recursive method to find a factorial using hardcoded and user input numbers---------*/
/*------------------------------------------------------Marks - Out of 10-------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
    class Program
    {
        static void Main(string[] args)
        /*Hardcoded and user input passed to the factorial recursive method(Marks out of 5)*/
        {
            Program help = new Program();
            int hard = 5;
            int user = 0;
            try
            {
                Console.WriteLine("please enter a number you wish to find the factorial");
                user = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("you didn't enter a number");
            }


            
            Console.WriteLine("the factor of your number is {0}", help.FindFactorial(user));
            Console.ReadLine();


        }

        private int FindFactorial(int user)/*Recursive method (Marks out of 5)*/
        {
            int factor = 0;
            if (user == 1)
            {
                return 1;
            }
            return factor = user * FindFactorial(user - 1);

            // factoral 5 = 5* factoral 4
            // factoral 4 = 4* factoral 3
            // factoral 2 = 3* factoral 2
            // factoral 2 = 2* factoral 1
            // factoral 1 = 1
        }
    }

/*------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------Question 2 END-------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
}
